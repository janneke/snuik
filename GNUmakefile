### Snuik --- An IRC bot using guile-8sync
### Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
###
### This file is part of Snuik.
###
### Snuik is free software; you can redistribute it and/or modify it
### under the terms of the GNU General Public License as published by
### the Free Software Foundation; either version 3 of the License, or (at
### your option) any later version.
###
### Snuik is distributed in the hope that it will be useful, but
### WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
###
### You should have received a copy of the GNU General Public License
### along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
###
### Commentary:
###
### Code:

.PHONY: all all-go check clean clean-go clean-log default update

default: all

###
### Config.
###
PACKAGE_NAME = Snuik
PACKAGE = snuik

GUILD = guild
GUILE = guile
GUIX = guix
PERL = perl

host = $(shell $(GUILE) -c '(display %host-type)')
srcdest =
srcdir = .


###
### Sanity.
###
SHELL = bash -o pipefail


###
### Common targets.
###
all: all-go
clean: clean-go clean-log


###
### Build.
###
GUILEC_FLAGS =					\
 -Warity-mismatch				\
 -Wformat					\
 --load-path=$(abs_top_srcdir)			\
 --load-path=$(abs_top_srcdir)/runtime/scheme

AM_V_GUILEC = $(AM_V_GUILEC_0$(V))
AM_V_GUILEC_ = $(AM_V_GUILEC_@0)
AM_V_GUILEC_0 = @echo "  GUILEC  " $@;

SCM_FILES = $(wildcard 				\
 8sync/systems/*.scm				\
 snuik.scm					\
 snuik/*.scm					\
 snuik/goops/*.scm				\
 snuik/handler/*.scm				\
 build-aux/test/automake.scm			\
 test/*.scm)
GO_FILES = $(SCM_FILES:%.scm=%.go)

all-go: $(GO_FILES)

clean-go:
	rm -f $(shell find . -name '*.go')

%.go:	%.scm
	$(AM_V_GUILEC)GUILE_AUTO_COMPILE=0			\
	$(GUILD) compile --target="$(host)" $(GUILEC_FLAGS)	\
	-o "$@" "$<"


###
### Check
###
TEST_FILES = $(wildcard test/*.scm test/handler/*.scm)
TEST_LOGS = $(TEST_FILES:%.scm=%.log)

check: $(TEST_LOGS)

$(foreach i,$(TEST_LOGS),$(eval $(i): $(GO_FILES)))
%.log:	%.scm
	$(GUILE) --no-auto-compile -L . -C . $< | tee $@

clean-log:
	rm -f $(shell find test -name '*.log')


###
### Distribution.
###
GPG_KEY_ID = 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

guix-pack-file = guix/pack/$(PACKAGE).scm
TEMP_NAME := $(shell mktemp --dry-run --tmpdir $(PACKAGE).XXXXXXXXXX)
BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
COMMIT := $(shell git show --format=%H --no-patch)
REVISION := $(shell grep -E 'revision "[0-9]+"' $(guix-pack-file) 2>/dev/null	\
  | grep -Eo '[0-9]+')
REVISION+1 := $(shell echo $$(( $(REVISION) + 1)))
GIT_VERSION := $(shell test -e ${srcdest}.git && (cd ${srcdir} && git describe --dirty 2>/dev/null) || cat ${srcdest}.tarball-version || echo 0.0)
TARBALL_VERSION := $(GIT_VERSION:v%=%)
VERSION := $(TARBALL_VERSION)

distdir = $(PACKAGE)-$(VERSION)
DIST_ARCHIVES = $(distdir).tar.gz

update-git-hash:
	git diff --exit-code HEAD
	git clone --branch=$(BRANCH) .git $(TEMP_NAME)
	sed -i										\
	    -e 's,(commit "[^"]*"),(commit "$(COMMIT)"),'				\
	    -e 's,(revision "[^"]*"),(revision "$(REVISION+1)"),'			\
	    -e 's,(base32 "[^"]*"),(base32 "'$$(guix hash -rx $(TEMP_NAME))'"),'	\
	    $(guix-pack-file)
	rm -rf $(TEMP_NAME)

release-git: update-git-hash
	git commit -m 'guix: Update to $(COMMIT).'			\
	    -m '* $(guix-pack-file) ($(PACKAGE)): Update to $(COMMIT).'	\
	    $(guix-pack-file)

# Be friendly to Debian; avoid using EPOCH
MTIME = $(shell git show HEAD --format=%ct --no-patch 2>/dev/null)
# Reproducible tarball
TAR_FLAGS =					\
 --sort=name					\
 --mtime=@$(MTIME)				\
 --owner=0					\
 --group=0					\
 --numeric-owner				\
 --mode=go=rX,u+rw,a-s

.tarball-version:
	echo $(COMMIT) > $@

GIT_ARCHIVE_HEAD = git archive HEAD --
GIT_LS_FILES = git ls-files
ifeq ($(wildcard .git),)
GIT_ARCHIVE_HEAD = tar -cf-
GIT_LS_FILES = find
MTIME = 0
endif

check-signature:
	git show HEAD --show-signature | grep 'gpg: Good signature'

dist: check-signature $(DIST_ARCHIVES)

sign-dist: $(DIST_ARCHIVES)
	gpg -a --output $(DIST_ARCHIVES).sig --detach-sig $(DIST_ARCHIVES)
	git checkout ChangeLog

tree-clean-p:
	test ! -d .git || git diff --exit-code > /dev/null
	test ! -d .git || git diff --cached --exit-code > /dev/null
	@echo commit:$(COMMIT)

generate-ChangeLog:
	$(PERL) ${srcdest}build-aux/gitlog-to-changelog --srcdir=${srcdir} > $@
	cat ChangeLog >> generate-ChangeLog
	mv generate-ChangeLog ChangeLog

$(DIST_ARCHIVES): .tarball-version | generate-ChangeLog
	($(GIT_LS_FILES)				\
	    --exclude=$(distdir);			\
	    echo $^ | tr ' ' '\n')			\
	    | tar $(TAR_FLAGS)				\
	    --transform=s,^,$(distdir)/,S -T- -cf-	\
	    | gzip -c --no-name > $@
	git checkout ChangeLog

update-hash: $(DIST_ARCHIVES)
	$(GUIX) download file://$(PWD)/$<
	sed -i													\
		-e 's,version #!$(PACKAGE)!# "[^"]*"),version #!$(PACKAGE)!# "$(VERSION)"),'			\
		-e 's,(base32 #!$(PACKAGE)!# "[^"]*"),(base32 #!$(PACKAGE)!# "$(shell $(GUIX) hash $<)"),'	\
		$(guix-pack-file)

tag:
	git tag -s v$(VERSION) -m "$(PACKAGE_NAME) $(VERSION)."

release: check-signature update-hash
	git commit -m 'guix: $(PACKAGE): Update to $(VERSION).'			\
	    -m '* $(guix-pack-file) ($(PACKAGE)): Update to $(VERSION).'	\
	    $(guix-pack-file)
	git checkout ChangeLog

sum-announce:
	sed -i																		\
	    -e "s,  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  $(distdir).tar.gz,  $(shell sha256sum $(distdir).tar.gz),"	\
	    -e "s,  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  $(distdir).tar.gz,  $(shell sha1sum $(distdir).tar.gz),"					\
	    -e "s,  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  $(distdir).tar.gz,  $(shell md5sum $(distdir).tar.gz),"						\
	doc/announce/ANNOUNCE-$(VERSION)

define HELP_TOP
Usage: make [OPTION]... [TARGET]...

Main and non-standard targets:
  all             update everything
  all-go          update .go files
  dist            update $(DIST_ARCHIVES)
  check           run unit tests
  clean           clean .go and .log files
  clean-log       clean .log files
  clean-go        clean .go files
  release         dist and tag
  sign-dist       sign dist
endef
export HELP_TOP
help:
	@echo "$$HELP_TOP"
