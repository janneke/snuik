;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (pack snuik)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix build-system guile)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages guile)
  #:use-module (pack guile-8sync))

(define-public snuik
  (let ((commit "21676ba5c3fdde9360cb9fe2f14cfc93e51823d0")
        (revision "2"))
    (package
      (name "snuik")
      (version (git-version "0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/janneke/snuik.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1w223k27ains8q0jww495h0721jkp762pkwbxkadygmmbxw1sjzq"))))
      (native-inputs (list guile-3.0))
      (inputs (list bash-minimal guile-3.0 guile-8sync-irc))
      (build-system guile-build-system)
      (arguments
       (list
        #:not-compiled-file-regexp "(guix|guix/.*)[.]scm$"
        #:modules '((srfi srfi-1)
                    (ice-9 popen)
                    (guix build guile-build-system)
                    (guix build utils))
        #:phases
        #~(modify-phases %standard-phases
            #$@(if (%current-target-system)
                   #~()
                   #~((add-after 'build 'check
                        (lambda _
                          (let* ((tests (find-files "test" "[.]scm$"))
                                 (guile #$(this-package-input "guile"))
                                 (guile (string-append guile "/bin/guile")))
                            (fold (lambda (test result)
                                    (and
                                     result
                                     (invoke guile "--no-auto-compile" test)))
                                  #t
                                  tests))))))
            (add-after 'build 'install-script
              (lambda _
                (let* ((bash #$(this-package-input "bash-minimal"))
                       (bash (string-append bash "/bin/bash"))
                       (guile #$(this-package-input "guile"))
                       (guile (string-append guile "/bin/guile"))
                       (build-guile #$(this-package-native-input "guile"))
                       (build-guile (string-append build-guile "/bin/guile"))
                       (guile-8sync #$(this-package-input "guile-8sync-irc"))
                       (out #$output)
                       (bin (string-append out "/bin"))
                       (effective (read
                                   (open-pipe* OPEN_READ
                                               build-guile "-c"
                                               "(write (effective-version))")))
                       (path (list (string-append guile "/bin")))
                       (scm-dir (string-append "/share/guile/site/" effective))
                       (scm-path (list (string-append out scm-dir)
                                       (string-append guile-8sync scm-dir)))
                       (go-dir (string-append "/lib/guile/" effective
                                              "/site-ccache/"))
                       (go-path (list (string-append out go-dir)
                                      (string-append guile-8sync go-dir))))
                  (mkdir-p "bin")
                  (copy-file "snuik.sh" "bin/snuik")
                  (substitute* "bin/snuik"
                    (("@SHELL@") bash))
                  (chmod "snuik" #o755)
                  (install-file "bin/snuik" bin)
                  (wrap-script (string-append out "/bin/snuik")
                    `("PATH" ":" prefix ,path)
                    `("GUILE_AUTO_COMPILE" ":" = ("0"))
                    `("GUILE_LOAD_PATH" ":" prefix ,scm-path)
                    `("GUILE_LOAD_COMPILED_PATH" ":" prefix ,go-path))))))))
      (home-page "https://gitlab.com/janneke/snuik")
      (synopsis "IRC bot using Guile-8sync")
      (description "@code{Snuik} is an IRC bot using the GNU 8sync (for
now).  It has some basic functionality only, such as seen, tell, and
what.")
      (license license:gpl3+))))
