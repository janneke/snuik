;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2018, 2019, 2020, 2023 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;
;; The GNU project defaults.  These are also the GNU Emacs defaults.
;; Re-asserting theme here, however, as a courtesy for setups that use
;; a global override.
;;
;;; Code

((nil . ((indent-tabs-mode . nil)
         (sentence-end-double-space . t)
         (tab-width   .  8)
         (fill-column . 72)
         (eval
          .
          (progn
            (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))))

 (makefile-mode
  .
  ((indent-tabs-mode . t)
   (eval
    .
    (progn
      (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)))))

 (scheme-mode
  .
  ((indent-tabs-mode . nil)
   (geiser-active-implementations . (guile))
   (eval
    .
    (progn
      (add-hook 'before-save-hook 'delete-trailing-whitespace nil t)
      (unless (boundp 'geiser-guile-load-path)
        (defvar geiser-guile-load-path '()))
      (defun prefix-dir-locals-dir (elt)
        (let* ((root-dir (locate-dominating-file buffer-file-name
                                                 ".dir-locals.el"))
               (root-dir (expand-file-name root-dir)))
          (concat root-dir elt)))
      (mapcar
       (lambda (dir) (add-to-list 'geiser-guile-load-path dir))
       (mapcar
        #'prefix-dir-locals-dir
        '("."))))))))
