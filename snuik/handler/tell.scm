;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2020, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler tell)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)

  #:use-module (ice-9 match)
  #:use-module ((oop goops) #:select (initialize))

  #:use-module (snuik goops display)
  #:use-module (snuik goops goops)

  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik store)
  #:use-module (snuik util))

(define-method (key (channel <string>) (recipient <string>))
  (string-append channel " " recipient))

(define-method (key (o <tell>))
  (key (.channel o) (.recipient o)))

(define %tell-responses
  '("Got it."
    "Okay."
    "Sure thing."
    "Will do."))

(define %deliver-preludes
  '("Welcome back ~a"
    "~a,"))

(define-class*-public <handler:tell> (<handler:with-store>)
  (tell-responses #:init-value %tell-responses)
  (deliver-preludes #:init-value %deliver-preludes))

(define-method (initialize (o <handler:tell>) initargs)
  (let ((initargs `(#:name "tell"
                    ,@initargs)))
    (next-method o initargs))
  o)

(define-method (init (o <handler:tell>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: init...\n" type)
    (flush-all-ports))
  (retrieve o))

(define-method (cleanup (o <handler:tell>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: cleaning up...\n" type)
    (flush-all-ports))
  (dump o))

(define (tell? message)
  (match (string-split message #\space)
    (((= strip-punctuation "later")
      (and action (= strip-punctuation
                     (? (cute member <> '("ask" "tell")))))
      (= strip-punctuation recipient)
      message ...)
     (let* ((message (string-join (match message
                                    ((":" message ...) message)
                                    (("," message ...) message)
                                    (_ message)))))
       (values recipient action message)))
    (_ (values #f #f #f))))

(define-method (handle-message (o <handler:tell>) (bot <irc-bot>) message)
  (let ((message-for-me (message-for-me? bot message)))
    (match (or message-for-me message)
      ((and ($ <irc:message>)
            (= irc:message-command 'PRIVMSG)
            (= irc:message-message line)
            (= irc:message-channel channel)
            (= irc:message-speaker speaker)
            (= irc:message-message message))
       (let ()
         (define (deliver tell)
           (format #f "~a, ~a says: ~a"
                   speaker (.speaker tell) (.message tell)))

         (define (get-tells key)
           (or (get (.store o) key)
               '()))

         (define (store tell)
           (let* ((key (key tell))
                  (tells (get-tells key))
                  (tells (append tells (list tell))))
             (put! (.store o) key tells)))

         (let* ((key (key channel speaker))
                (tells (get-tells key))
                (tells
                 (if (null? tells) '()
                     ;; Deliver any messages for speaker
                     (let* ((count (length tells))
                            (message (format #f " you have ~a"
                                             (if (= count 1) "1 message"
                                                 (format #f "~a messages" count))))
                            (preludes (map (compose (cute string-append <> message)
                                                    (cute format #f <> speaker))
                                           (.deliver-preludes o)))
                            (messages (map deliver tells))
                            (message (string-join messages "\n"))
                            (tells (map (cute string-append <> "\n" message)
                                        preludes)))
                       (erase! (.store o) key)
                       tells)))
                (responses
                 (if (not message-for-me) '()
                     ;; Store a message (tell) for later delivery.
                     (let ((recipient action message (tell? message)))
                       (and recipient
                            (let ((tell (make <tell>
                                          #:channel channel
                                          #:speaker speaker
                                          #:recipient recipient
                                          #:message message)))
                              (format (current-error-port)
                                      "~a asked us to ~a ~a: ~a\n" speaker action
                                      recipient message)
                              (flush-all-ports)
                              (store tell)
                              (.tell-responses o))))))
                (messages (list tells responses))
                (messages (filter pair? messages)))
           (and (pair? messages)
                messages))))
      (_ #f))))
