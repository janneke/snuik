;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler feed)
  #:use-module (ice-9 match)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik util))

(define %feed-responses
  '(":-)"
    ":)"
    "(y)"
    "ty!"))

(define-class*-public <handler:feed> (<handler>)
  (feed-responses #:init-value %feed-responses))

(define (feed? message)
  (match (string-split message #\space)
    (((and food (= strip-punctuation "botsnack")))
     food)
    (_ (string-split message #\space) #f)))

(define-method (handle-message-for-me (o <handler:feed>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-message message)
          (= irc:message-speaker speaker))
     (and (and=> message feed?)
          (.feed-responses o)))))
