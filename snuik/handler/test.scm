;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2020, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler test)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module ((oop goops) #:select (slot-set!))
  #:use-module (8sync)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik store)
  #:use-module (snuik util))

(define irc-bot-socket (@@ (8sync systems irc) irc-bot-socket))

(define %test-channel "#test")

(define %talk-responses
  '("I'm Groot!"))

(define %lurk-responses
  '("just lurking here..."))

(define* (compare-reply reply expected #:key nick speaker talking-to-me?)
  (match expected
    ((and (? string?) expected)
     (equal? reply expected))
    (((and (? string?) expected) ...)
     (member reply expected))
    (_ #f)))

(define-class*-public <test> ()
  (name #:init-value "0")
  (channel #:init-value %test-channel)
  (message #:init-value "hello snuik!")
  (reply #:init-value "Oh hi")
  (compare #:init-value compare-reply))

(define-class*-public <handler:test> (<handler>)
  (channel #:init-value %test-channel)
  (snuik?)
  (talk-responses #:init-value %talk-responses)
  (lurk-responses #:init-value %lurk-responses)
  (tests #:init-value (list))
  (kill? #:init-value #t)
  (pass #:init-value 0)
  (fail #:init-value 0)
  (log #:init-value '()))

(define-method (init (o <handler:test>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: init...\n" type)
    (flush-all-ports)))

(define RPL_REPLY "353")

(define-method (send-names (o <handler:test>) (bot <irc-bot>) channel)
  (irc:names (irc-bot-socket bot) channel))

(define-method (append-log (o <handler:test>) template . rest)
  (let ((line (apply format #f template rest)))
    (slot-set! o 'log (append (.log o) (list line)))))

(define-method (run-handler (o <handler:test>) (bot <irc-bot>) irc-message)
  (let* ((message-for-me (message-for-me? bot irc-message))
         (message (or message-for-me irc-message)))
    (match message
      ((and ($ <irc:message>)
            (= irc:message-command command)
            (= irc:message-channel channel)
            (= irc:message-speaker speaker)
            (= irc:message-message message))
       (case command
         ((PRIVMSG)
          (match (.tests o)
            ((($ <test> name channel _ expected compare) rest ...)
             (slot-set! o 'tests rest)
             (let* ((message (irc:message-message irc-message))
                    (pass? (compare message expected
                                    #:nick (.nick bot)
                                    #:speaker speaker
                                    #:talking-to-me? message-for-me)))
               (append-log o "test: ~a\n" name)
               (append-log o "expected: ~s\n" expected)
               (append-log o "actual: ~s\n" message)
               (append-log o "result:~a\n" (if pass? "pass" "fail"))
               (append-log o "\n")
               (if pass? (slot-set! o 'pass (1+ (.pass o)))
                   (slot-set! o 'fail (1+ (.fail o)))))
             (match (.tests o)
               ((($ <test> name channel message reply compare) rest ...)
                (send-message bot channel message))
               (_ (and (.kill? o) (kill (getpid) SIGINT)))))

            (_ (and (.kill? o) (kill (getpid) SIGINT)))))
         (else
          (match irc-message
            ((and ($ <irc:message>)
                  (= irc:message-command command)
                  (= irc:message-message message)
                  (= irc:message-channel (? (cute equal? <> (.nick bot))))
                  (= irc:message-speaker nick)
                  (= irc:message-speaker #f))
             (let* ((type (class-name (class-of o)))
                    (nick (.nick bot))
                    (pass (.password bot))
                    (type (class-name (class-of o)))
                    (words (string-split message #\space))
                    (channel (.channel o)))
               (match command
                 (353                   ;RPL_NAMREPLY
                  (match words
                    (((= strip-punctuation nicks) ...)
                     (cond
                      ((member "snuik" nicks)
                       (slot-set! o 'snuik? #t)
                       (format (current-error-port) "snuik is here!\n")
                       (flush-all-ports)
                       (match (.tests o)
                         ((($ <test> name channel message) rest ...)
                          (format (current-error-port) "running test: ~a\n" name)
                          (flush-all-ports)
                          (send-message bot (.channel o) message))
                         (_ (and (.kill? o) (kill (getpid) SIGINT)))))
                      (else
                       (format (current-error-port) "waiting for snuik...\n")
                       (flush-all-ports)
                       (8sleep 5))))
                    (_ #t)))
                 (366                   ;RPL_ENDOFNAMES
                  (unless (.snuik? o)
                    (format (current-error-port) "waiting for snuik...\n")
                    (flush-all-ports)
                    (send-names o bot (.channel o)))
                  #f)
                 (_ #f))))
            ((and ($ <irc:message>)
                  (= irc:message-channel channel)
                  (= irc:message-speaker nick))
             #f)
            (_ #f)))))
      (_ #f))))

(define-method (default-handler (o <handler:test>) (bot <irc-bot>) irc-message)
  (match irc-message
    ((and ($ <irc:message>)
          (= irc:message-channel channel)
          (= irc:message-speaker speaker)
          (= irc:message-message line)
          (= irc:message-message message))
     (let ((message-for-me (message-for-me? bot irc-message)))
       (define (reply message)
         (let ((reply-channel (if (equal? channel (.nick bot)) speaker
                                  channel)))
           (send-message bot reply-channel message)))

       (when (string-suffix? "test: kill" line)
         (format (current-error-port) "killing test-bot...\n")
         (flush-all-ports)
         (reply "goodbye cruel world...")
         (and (.kill? o) (kill (getpid) SIGINT)))

       (if message-for-me (.talk-responses o)
           (let ((lurk-responses (.lurk-responses o)))
             (and (pair? lurk-responses) lurk-responses)))))))

(define-method (handle-message (o <handler:test>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-message line))
     (let ((type (class-name (class-of o))))
       (format (current-error-port) "[~a]: privmsg: ~a\n" type line)
       (flush-all-ports))))

  (let ((result (run-handler o bot message)))
    (or result
        (and (null? (.tests o))
             (default-handler o bot message)))))
