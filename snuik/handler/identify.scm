;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler identify)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)

  #:use-module (ice-9 match)

  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik util))

(define-class*-public <handler:identify> (<handler>))

(define-method (handle-message (o <handler:identify>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-command 'NOTICE)
          (= irc:message-speaker "NickServ")
          (= irc:message-channel (? (cute equal? <> (.nick bot))))
          (= irc:message-message line)
          (= irc:message-message message))
     (let* ((nick (.nick bot))
            (pass (.password bot))
            (identify! "This nickname is registered.")
            (type (class-name (class-of o))))
       (let* ((request? (string-prefix? identify! message))
              (identify? (and request?
                              (string? pass))))
         (cond
          (identify?
           (let ((pass (make-string (string-length pass) #\*)))
             (format (current-error-port) "[~a]: IDENTIFY ~a ~a...\n"
                     type nick pass))
           (flush-all-ports)
           (send-message bot "NickServ" (format #f "IDENTIFY ~a ~a" nick pass)))
          (else
           (cond
            ((not (string? pass))
             (format (current-error-port) "[~a]: Skipping, got no password.\n"
                     type)
            (flush-all-ports))
            (else
             (format (current-error-port) "[~a]: Skipping, other: ~a\n" type
                     line)
            (flush-all-ports)))
           #f)))))
    (_ #f)))
