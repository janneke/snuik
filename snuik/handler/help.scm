;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler help)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 string-fun)

  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik util))

(define %help-introductions             ;nick real-name
  '("Hello, I'm ~a, a ~a."
    "I respond to some natural language commands, such as:"))

(define %help-alist
  '(("help" "You're doing it.")
    ("ask" "\"later ask <nick> <something>\" asks me to deliver a message to them next time they speak.")
    ("botsnack" "Thank me; feed me.")
    ("hello" "Let's say hi!")
    ("tell" "\"later tell <nick> <message>\" asks me to deliver a message to them next time they speak.")
    ("seen" "\"seen <nick>\" asks for the last time I saw them speak.")
    ("what is" "\"what is <thing>\" asks me for information.")
    ("uptime" "Print bot time.")))

(define %help-responses                 ;speaker nick
  `("Don't ask me ~a, I'm just a bot!"
    "For help on my commands, use /msg <snuik> help"
    "~a, just ask nicely: /msg <snuik> help"
    "~a, you only need to ask: /msg <snuik> help"
    "Not sure how to help you, ~a"))

(define-class*-public <handler:help> (<handler>)
  (help-introductions #:init-value %help-introductions)
  (help-alist #:init-value %help-alist)
  (help-responses #:init-value %help-responses))

(define (help? message)
  (match (string-split message #\space)
    (((and help (= strip-punctuation "help")) (or "?" "commands") ...)
     help)
    (_ #f)))

(define-method (handle-message-for-me (o <handler:help>) (bot <irc-bot>) message)
  (define (try-format template . args)
    (let ((template (or (false-if-exception
                         (apply format #f template args))
                        template)))
      (string-replace-substring template "<snuik>" (.nick bot))))

  (match message
    ((and ($ <irc:message>)
          (= irc:message-channel channel)
          (= irc:message-message message)
          (= irc:message-speaker speaker))
     (let ((nick (.nick bot))
           (real-name (.real-name bot)))
       (and (help? message)
            (cond
             ;; Send help to private channel.
             ((priv-msg? bot channel)
              (let* ((intro (map (compose (cute try-format <> nick real-name))
                                 (.help-introductions o)))
                     (help (map (cute apply string-join <> '(" - "))
                                (.help-alist o)))
                     (lines (append intro help)))
                (string-join lines "\n")))
             ;; Send help responses to public channel.
             (else
              (map (cute try-format <> speaker) (.help-responses o)))))))))
