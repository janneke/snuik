;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler seen)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module ((oop goops) #:select (initialize))

  #:use-module (snuik goops display)
  #:use-module (snuik goops goops)

  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik store)
  #:use-module (snuik time)
  #:use-module (snuik util))

(define %seen-preludes
  '("I last saw ~a"
    "I think I remember ~a"
    "~a?, pretty sure was seen"
    "~a was last seen"
    "~a was"))

(define %not-seen-responses
  '("Sorry, haven't seem 'em."
    "Nope."
    "Sorry, no."
    "Not as far as I can remember.")  )

(define-class*-public <handler:seen> (<handler:with-store>)
  (seen-preludes #:init-value %seen-preludes)
  (not-seen-responses #:init-value %not-seen-responses))

(define-method (initialize (o <handler:seen>) initargs)
  (let ((initargs `(#:name "seen"
                    ,@initargs)))
    (next-method o initargs))
  o)

(define-method (init (o <handler:seen>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: init...\n" type)
    (flush-all-ports))
  (retrieve o))

(define-method (cleanup (o <handler:seen>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: cleaning up...\n" type)
    (flush-all-ports))
  (dump o))

(define-method (get-seen (o <handler:seen>) nick)
  (get (.store o) nick))

(define-method (set-seen! (o <handler:seen>) speaker channel message emote?)
  (let* ((type (if emote? 'emoting 'saying))
         (seen (make <seen> #:nick speaker #:channel channel
                     #:time (current-time) #:type type #:message message)))
    (put! (.store o) speaker seen)))

(define (seen? message)
  (define strip strip-punctuation)
  (match (string-split message #\space)
    ((or ((= strip "seen") nick (or "?" (= strip "lately")) ...)
         (nick (= strip "seen") (or "?" (= strip "lately")) ...))
     (strip nick))
    (_ #f)))

(define-method (handle-message (o <handler:seen>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-command 'PRIVMSG)
          (= irc:message-channel channel)
          (= irc:message-message line)
          (= irc:message-speaker speaker)
          (= irc:message-emote? emote?)
          (= irc:message-private? private?))
     (let ()
      (define (update-seen-)
        (set-seen! o speaker channel line emote?))

      (unless private?
        (update-seen-))
      ;; line not (really) handled
      #f))
    (_ #f)))

(define-method (handle-message-for-me (o <handler:seen>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-channel channel)
          (= irc:message-message line)
          (= irc:message-message message)
          (= irc:message-speaker speaker)
          (= irc:message-emote? emote?)
          (= irc:message-private? private?))
     (let ()
      (define (get-seen- nick)
        (get-seen o nick))

      (define (update-seen-)
        (set-seen! o speaker channel line emote?))

      (let ((result
             (let ((nick (seen? message)))
               (and nick
                    (match (get-seen- nick)
                      (($ <seen> nick seen-channel time type message)
                       (let* ((time (if (string? time) (string->number time)
                                        time))
                              (ago (format-time time)))
                         (map (cute format #f "~a in ~a ~a ago, ~a: ~a"
                                    <> seen-channel ago type message)
                              (map (cute format #f <> nick)
                                   (.seen-preludes o)))))
                      (#f (.not-seen-responses o)))))))
        (unless private?
          (update-seen-))
        ;; Result determines handled.
        result)))))
