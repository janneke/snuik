;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2020, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler whut)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik snuik))

(define %whut-responses
  '("*innocent puppy look*"
    "Does not compute."
    "Err..."
    "If you say so..."
    "Heu..."
    "Whatever"
    "Whut?"))

(define-class*-public <handler:whut> (<handler>)
  (whut-responses #:init-value %whut-responses))

(define-method (handle-message-for-me (o <handler:whut>) (bot <irc-bot>) message)
  (.whut-responses o))
