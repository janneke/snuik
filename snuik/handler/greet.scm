;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler greet)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik util))

(define %greet-responses
  '("Oh hi ~a!"))

(define %morning-responses
  '("Good morning, ~a."
    "Morning, ~a."
    "And a good morning, to you too, ~a."))

(define %night-responses
  '("Good night, ~a."
    "Later, ~a."
    "See you, ~a."))

(define-class*-public <handler:greet> (<handler>)
  (greet-responses #:init-value %greet-responses)
  (morning-responses #:init-value %morning-responses)
  (night-responses #:init-value %night-responses))

(define (greet? line)
  (match (string-split line #\space)
    (((and greet (= strip-punctuation (or "hello" "hey" "hi" "greetings"))))
     greet)
    (_ #f)))

(define (morning? line)
  (match (string-split line #\space)
    (("good" ...
      (and morning (= strip-punctuation
                      (or "morning" "mornin" "mornin" "moin"
                          "goodmorning"))))
     morning)
    (_ #f)))

(define (night? line)
  (match (string-split line #\space)
    (("good" ... (and night (= strip-punctuation
                               (or "night" "nite"
                                   "goodnight"))))
     night)
    (_ #f)))

(define-method (handle-message-for-me (o <handler:greet>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-message message)
          (= irc:message-speaker speaker))
     (and message
          (let ((responses (or (and (greet? message)
                                    (.greet-responses o))
                               (and (morning? message)
                                    (.morning-responses o))
                               (and (night? message)
                                    (.night-responses o)))))
            (and (pair? responses)
                 (map (cute format #f <> speaker) responses)))))))
