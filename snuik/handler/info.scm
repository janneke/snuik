;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2020, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler info)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)

  #:use-module (ice-9 match)
  #:use-module ((oop goops) #:select (initialize))

  #:use-module (snuik goops display)
  #:use-module (snuik goops goops)
  #:use-module (snuik goops serialize)
  #:use-module (snuik goops unserialize)

  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik store)
  #:use-module (snuik util))

(define %what-preludes
  '("I've heard "
    "Someone once said "
    ""
    "I could be wrong, but "
    "Its been said that "
    "Last time I checked "
    "From what I understand, "))

(define %remember-responses
  '("Got it."
    "Okay."
    "Understood."
    "I'll keep that in mind."
    "So noted."))

(define %know-responses
  '("I knew that!"
    "I know, I know..."
    "So what's new?"
    "That's what they say..."
    "Yeah..."))

(define %change-responses
  '("Alright, I'll change my mind."
    "Did I say something different?"
    "Okay, If you say so."
    "Times are changing, eh?"))

(define %forget-responses
  `("Okay."
    "Consider it forgotten."
    "What were we talking about?"))

(define %amnesia-responses
  `("Already done."
    "Never knew it."
    "What is this ~a you say?"
    "~a, never heard of it!"))

(define-class*-public <handler:info> (<handler:with-store>)
  (forget-store #:init-form (make <store> #:name "forget"))
  (what-preludes #:init-value %what-preludes)
  (remember-responses #:init-value %remember-responses)
  (know-responses #:init-value %know-responses)
  (change-responses #:init-value %change-responses)
  (forget-responses #:init-value %forget-responses)
  (amnesia-responses #:init-value %amnesia-responses))

(define-method (initialize (o <handler:info>) initargs)
  (let ((initargs `(#:name "info"
                    ,@initargs)))
    (next-method o initargs))
  (let ((initargs `(#:name ,(.name (.forget-store o))
                    ,@initargs)))
    (initialize (.forget-store o) initargs))
  o)

(define-method (init (o <handler:info>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: init...\n" type)
    (flush-all-ports))
  (retrieve o))

(define-method (cleanup (o <handler:info>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: cleaning up...\n" type)
    (flush-all-ports))
  (dump o))

(define-method (dump (o <handler:info>))
  (next-method)
  (dump (.forget-store o)))

(define-method (retrieve (o <handler:info>))
  (next-method)
  (retrieve (.forget-store o)))

(define-method (forget (o <handler:info>) thing)
  (let ((message (lookup o thing)))
    (when message
      (erase! (.store o) thing))
    (let ((memory (or (get (.forget-store o) thing)
                      '())))
      (unless (member message memory)
        (let ((memory (cons message memory)))
          (put! (.forget-store o) thing memory))))
    message))

(define-method (lookup (o <handler:info>) thing)
  (get (.store o) thing))

(define-method (remember (o <handler:info>) thing message)
  (put! (.store o) thing message))

(define (what? message)
  "Parse MESSAGE and, if it is an info request, return the thing."
  (and=> (match (string-split message #\space)
           ((or ((and thing (? (cute string-suffix? "?" <>))))
                (thing "?")
                ("what" thing)
                ((and thing (? (cute string-suffix? "?" <>))))
                ("what" thing "?")
                ("what" "is" thing)
                ("what" "is" thing "?")
                ("what" "is" (and thing (? (cute string-suffix? "?" <>)))))
            thing)
           (_ #f))
         strip-punctuation))

(define (remember? message)
  "Parse MESSAGE and, if it is a remember instruction, return thing and
message."
  (match (string-split message #\space)
    ;; Avoid remembering no thing.
    ((or ("remember" thing)
         (thing "is"))
     (values #f #f))

    ;; Avoid remember thing is massage
    ((or ("remember" thing message ...)
         (thing "is" message ...))
     (=> failure)
     (let ((message (string-join (match message
                                   ((":" message ...) message)
                                   (("," message ...) message)
                                   (_ message)))))
       (if (string-null? message) (values #f #f)
           (values thing message))))

    (_ (values #f #f))))

(define (forget? message)
  "Parse MESSAGE and, if it is a forget instruction, return thing."
  (match (string-split message #\space)
    ;; Avoid forgetting no thing.
    (((= strip-punctuation "forget"))
     #f)
    ;; Forget some info.
    (((= strip-punctuation "forget") thing)
     thing)
    (_ #f)))

(define-method (handle-message-for-me (o <handler:info>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-channel channel)
          (= irc:message-message message))
     (let ((thing (what? message)))

       (define (forget- thing)
         (forget o thing))

       (define (lookup- thing)
         (or (lookup o (string-append channel "-" thing))
             (lookup o (string-append (strip-hash channel) "-" thing))
             (lookup o thing)))

       (define (remember- thing message)
         (let ((current (lookup o thing)))
           (unless (equal? current message)
             (when current
               (forget- thing))
             (remember o thing message)))
         message)

       (define (try-format template . args)
         (or (false-if-exception
              (apply format #f template args))
             template))

       (or
        ;; Request info about thing.
        (and thing
             (let ((message (lookup- thing)))
               (if (not message) (string-append thing "?")
                   (map (cute string-append <> message)
                        (.what-preludes o)))))

        ;; Remember that thing is message.
        (let ((thing message (remember? message)))
          (and thing
               (let ((info (lookup- thing)))
                 (if (equal? info message) (.know-responses o)
                     (begin
                       (remember- thing message)
                       (if info (.change-responses o)
                           (.remember-responses o)))))))

        ;; Forget some info.
        (let ((thing (forget? message)))
          (and thing
               (if (forget- thing) (.forget-responses o)
                   (map (cute try-format <> thing) (.amnesia-responses o))))))))))
