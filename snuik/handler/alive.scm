;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2025 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler alive)
  #:use-module (ice-9 match)
  #:use-module ((oop goops) #:select (initialize))

  #:use-module (8sync systems irc)
  #:use-module (snuik goops goops)

  #:use-module (snuik handler)
  #:use-module (snuik irc)
  #:use-module (snuik snuik)
  #:use-module (snuik util)
  #:export (alive?))

(define irc-bot-socket (@@ (8sync systems irc) irc-bot-socket))

(define-class-public <handler:alive> (<handler>)
  (response #:accessor .response #:init-value (current-time)))

(define-method (initialize (o <handler:alive>) initargs)
  (let ((initargs `(#:name "alive"
                    ,@initargs)))
    (next-method o initargs))
  o)

(define-method (alive? (o <handler:alive>) (bot <irc-bot>) seconds)
  (let ((channel (car (irc-bot-channels bot)))
         (user (irc-bot-username bot))
         (socket (irc-bot-socket bot))
         (interval (- (current-time) (.response o))))
    (irc:ping socket channel user)
    (<= interval seconds)))

(define-method (handle-message (o <handler:alive>) (bot <irc-bot>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-command 'PONG))
     (set! (.response o) (current-time))
     #t)
    (_ #f)))
