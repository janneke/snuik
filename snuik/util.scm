;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2017 Rutger van Beusekom <rutger@dezyne.org>
;;; Copyright © 2020, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 1996, 2004, 2009, 2010, 2013 Free Software Foundation, Inc.
;;; Copyright © 2012 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik util)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:export (alist->hash-table
            conjoin
            disjoin
            false-if-exception*
            hash-table->alist
            match:positions
            match:substrings
            mkdir-p
            password
            pke
            random-element
            strip-hash
            strip-punctuation))

(define (disjoin . predicates)
  "Like OR but for predicates:
  (filter (disjoin zero? odd?) '(0 1 2 3))
 => '(0 1 3)
"
  (lambda arguments
    (any (cute apply <> arguments) predicates)))

(define (conjoin . predicates)
  "Like AND but for predicates:
  (find (conjoin even? (negate zero?)) '(0 1 2 3))
 => '2
"
  (lambda arguments
    (every (cute apply <> arguments) predicates)))

(define (pke . stuff)
  "Like peek (pk), writing to (CURRENT-ERROR-PORT)."
  (newline (current-error-port))
  (display ";;; " (current-error-port))
  (write stuff (current-error-port))
  (newline (current-error-port))
  (car (last-pair stuff)))

(define (alist->hash-table alist)
  (let ((table (make-hash-table (length alist))))
    (for-each (lambda (entry)
                (let ((key (car entry))
                      (value (cdr entry)))
                  (hash-set! table key value)))
              alist)
    table))

(define (hash-table->alist table)
  (hash-map->list cons table))

(define-syntax false-if-exception*
  (syntax-rules ()
    ((_ expr)
     (false-if-exception expr))
    ((_ expr #:warning template arg ...)
     (false-if-exception expr #:warning template arg ...))
    ((_ expr #:backtrace? backtrace? #:warning template arg ...)
     (catch #t
       (lambda _ expr)
       (const #f)
       (lambda (key . args)
         (let ((stack (make-stack #t)))
           (when backtrace?
             (display-backtrace stack (current-warning-port)))
           (for-each (lambda (s)
                       (if (not (string-null? s))
                           (format (current-warning-port) ";;; ~a\n" s)))
                     (string-split
                      (call-with-output-string
                        (lambda (port)
                          (format port template arg ...)
                          (print-exception port #f key args)))
                      #\newline)))
         #f)))))

(define (mkdir-p dir)
  "Create directory DIR and all its ancestors."
  (define absolute?
    (string-prefix? "/" dir))

  (define not-slash
    (char-set-complement (char-set #\/)))

  (let loop ((components (string-tokenize dir not-slash))
             (root       (if absolute?
                             ""
                             ".")))
    (match components
      ((head tail ...)
       (let ((path (string-append root "/" head)))
         (catch 'system-error
           (lambda ()
             (mkdir path)
             (loop tail path))
           (lambda args
             (if (= EEXIST (system-error-errno args))
                 (loop tail path)
                 (apply throw args))))))
      (() #t))))

(define (password nick)
  (let ((file-name (string-append ".password." nick)))
    (false-if-exception*
     (with-input-from-file file-name
       (compose string-trim-both read-line))
     #:backtrace? #f
     #:warning "cannot read password for ~a from ~a" nick file-name)))

(define (random-element lst)
  (list-ref lst (random (length lst))))

(define (strip-hash string)
  (let ((m (string-match "^[ #]*([^ #]*)$" string)))
    (if (not m) string
        (match:substring m 1))))

(define (strip-punctuation string)
  (let ((m (string-match "^[ ';:,.?!]*([^ ';:,.?!]*)[ ';:,.?!]*$" string)))
    (if (not m) string
        (match:substring m 1))))

(define (match:positions m)
  "If string-match M succeeded, return the positions of its substring
matches as a list."
  (and m
       (match (vector->list m)
         ((string positions ...)
          positions))))

(define (match:substrings m)
  "If string-match M succeeded, return its substrings as a list."
  (and m
       (let ((lst (vector->list m)))
         (map (cute match:substring m <>) (iota (1- (length lst)))))))
