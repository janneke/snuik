;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik snuik)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-71)

  #:use-module (ice-9 match)

  #:use-module (8sync)
  #:use-module (8sync systems irc)
  #:use-module ((oop goops) #:select (slot-ref))

  #:use-module (snuik handler)
  #:use-module (snuik handler alive)
  #:use-module (snuik irc)
  #:use-module (snuik goops goops)
  #:use-module (snuik goops util)
  #:use-module ((snuik store) #:select (.nick))
  #:use-module (snuik util)

  #:export (.actions
            .handlers
            .password
            .real-name
            message-for-me?
            ping
            priv-msg?
            send-line
            send-message
            send-one
            send-result
            talking-to-me?)
  #:re-export (<irc-bot>
               %irc:default-port
               .nick))

;;;
;;; 8sync imports and overrides.
;;;
(define-method (.nick (o <irc-bot>)) (irc-bot-username o))
(define-method (.real-name (o <irc-bot>)) (slot-ref o 'realname))


;;;
;;; Snuik.
;;;
(define-class-public <snuik> (<irc-bot>)
  (actions #:allocation #:each-subclass
           #:init-thunk (build-actions (*init* irc-bot-init)
                                       (*cleanup* irc-bot-cleanup)
                                       (ping ping)))
  (handlers #:init-keyword #:handlers #:init-form (list) #:accessor .handlers)
  (password #:init-keyword #:password #:init-value #f #:getter .password))

(define-method (irc-bot-init (o <snuik>) message)
  (format (current-error-port) "init...\n")
  (next-method)
  (flush-all-ports)
  (for-each (cute init <> o) (.handlers o))
  (flush-all-ports))

(define-method (irc-bot-cleanup (o <snuik>) message)
  (format (current-error-port) "cleaning up...\n")
  (flush-all-ports)
  (for-each (cute cleanup <> o) (.handlers o))
  (flush-all-ports)
  (next-method))

(define-method (ping (o <snuik>) message . rest)
  (format (current-error-port) "pinged; dump handlers with store...\n")
  (let* ((handlers (.handlers o))
         (alive (find (is? <handler:alive>) handlers)))
    (for-each dump (filter (is? <handler:with-store>) handlers))
    (unless (and alive (apply alive? alive o rest))
      (format (current-error-port) "not alive, exiting...\n")
      (kill (getpid) SIGTERM))))


;;;
;;; Util.
;;;
(define-method (priv-msg? (o <snuik>) channel)
  (equal? channel (.nick o)))

(define-method (talking-to-me? (o <snuik>))
  (lambda (recipient)
    (let ((nick (.nick o))
          (recipient (strip-punctuation recipient)))
      (equal? recipient nick))))

(define-method (talking-to-me? (o <snuik>) (words <pair>))
  (match words
    (((? (talking-to-me? o)) words ...)
     (string-join words))
    ((words ... (? (talking-to-me? o)))
     (string-join words))
    ((word)
     #f)
    ((word words ...)
     (let ((result (talking-to-me? o words)))
       (and (string? result)
            (string-append word " " result))))
    (_ #f)))

(define-method (talking-to-me? (o <snuik>) (message <top>)) ;GOOPS, learn about records!
  (let ((nick (.nick o)))
    (match message
      ((and ($ <irc:message>)
            (= irc:message-channel channel)
            (= irc:message-message message))
       (or (and (equal? nick channel)
                message)
           (let ((words (string-split message #\space)))
             (talking-to-me? o words)))))))

(define-method (message-for-me? (o <snuik>) (irc-message <top>)) ;GOOPS, learn about records!
  (match irc-message
    ((and ($ <irc:message>)
          (= irc:message-command 'PRIVMSG))
     (let ((message-for-me (talking-to-me? o irc-message)))
       (and message-for-me
            (set-field irc-message (irc:message-message) message-for-me))))
    (_ #f)))

(define-method (reply-channel (o <snuik>) message)
  (match message
    ((and ($ <irc:message>)
          (= irc:message-channel channel)
          (= irc:message-speaker speaker))
     (if (priv-msg? o channel) speaker
         channel))))

(define-method (send-line (o <snuik>) channel line)
  (<- (actor-id o) 'send-line channel line)
  ;; Handled.
  #t)

(define-method (send-message (o <snuik>) channel (message <string>))
  (let* ((lines (string-split message #\newline))
         (count (length lines))
         (lines (if (< count) lines
                    (begin
                      (format (current-error-port)
                              "snuik: attempting to send ~a lines, truncated\n"
                              count)
                      (take lines 10)))))
    (for-each (cute send-line o channel <>) lines))
  ;; Handled.
  #t)

(define-method (send-one (o <snuik>) channel (messages <pair>))
  (send-message o channel (random-element messages)))

(define-method (send-result (o <snuik>) channel result)
  (match result
    ((and message (? string?))
     (send-message o channel message))
    (((and choices (? string?)) ...)
     (send-one o channel choices))
    ((and multiple-choices (((? string?) ...) ...))
     (for-each (cute send-result o channel <>) multiple-choices))
    (_ result)))


;;;
;;; Dispatch handlers.
;;;
(define-method (handle-message (o <snuik>) irc-message)
  (match irc-message
    ((and ($ <irc:message>)
          (= irc:message-message message))
     (let* ((reply-channel (reply-channel o irc-message))
            (message-for-me (message-for-me? o irc-message))
            (result
             (fold (lambda (handler result)
                     (or result
                         (and message-for-me
                              (handle-message-for-me handler o message-for-me))
                         (handle-message handler o irc-message)))
                   #f
                   (.handlers o))))
       (send-result o reply-channel result)))
    (_
     (format (current-error-port)
             "programming-error: not an <irc:message>: ~a" irc-message)
     #f)))
