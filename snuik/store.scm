;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik store)
  #:use-module (srfi srfi-26)

  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module ((oop goops) #:select (initialize slot-set!))

  #:use-module (snuik goops display)
  #:use-module (snuik goops goops)
  #:use-module (snuik goops serialize)
  #:use-module (snuik goops unserialize)
  #:use-module (snuik util)

  #:export (dump
            erase!
            get
            retrieve
            put!))

(define-class*-public <tell> ()
  (channel)
  (speaker)
  (recipient)
  (message))

(define-unserialize-class <tell>)

(define-class*-public <seen> ()
  (nick)
  (channel)
  (time)
  (type)
  (message))

(define-unserialize-class <seen>)

(define (unserialize text)
  (let ((module (resolve-module '(snuik store))))
    (eval text module)))

(define-method (serialize-entry key (value <top>))
  (display "\n  (cons ")
  (write key)
  (display " ")
  (serialize value (current-output-port))
  (display ")")
  (force-output))

(define-method (serialize-entry key (value <pair>))
  (display "\n  (list ")
  (write key)
  (display " ")
  (serialize value (current-output-port))
  (display ")")
  (force-output))

(define-class*-public <store> ()
  (directory #:init-value "store")
  (dirty?)
  (name #:init-value "store")
  (table #:init-form (make-hash-table)))

(define-method (initialize (o <store>) initargs)
  (next-method)
  (let-keywords initargs #t
                ((name #f)
                 (store-directory #f))
    (when store-directory
      (let ((store-directory (and (string? store-directory)
                                  store-directory)))
        (slot-set! o 'directory store-directory))
      (when (string? name)
        (slot-set! o 'name name))))
  o)

(define-method (put! (o <store>) key value)
  (let ((current (get o key)))
    (unless (equal? value current)
      (hash-set! (.table o) key value)
      (slot-set! o 'dirty? #t)))
  value)

(define-method (erase! (o <store>) key)
  (let ((value (get o key)))
    (and value
         (hash-remove! (.table o) key)
         (slot-set! o 'dirty? #t)
         value)))

(define-method (get (o <store>) key)
  (hash-ref (.table o) key))

(define-method (file-name (o <store>))
  (string-append (.directory o) "/" (.name o) ".scm"))

(define-method (dump (o <store>))
  (when (and (.directory o) (.dirty? o))
    (let ((file-name (file-name o)))
      (format (current-error-port) "dumping store ~a...\n" file-name)
      (flush-all-ports)
      (mkdir-p (.directory o))
      (let ((tmp (string-append file-name "-")))
        (with-output-to-file tmp
          (lambda _
            (display "(list")
            (hash-for-each serialize-entry (.table o))
            (display ")\n")
            (force-output)))
        (rename-file tmp file-name)
        (slot-set! o 'dirty? #f)))))

(define-method (retrieve (o <store>))
  (if (not (.directory o)) '()
      (let ((file-name (file-name o)))
        (format (current-error-port) "retrieving store ~a...\n" file-name)
        (flush-all-ports)
        (when (file-exists? file-name)
          (let* ((text (with-input-from-file file-name read))
                 (alist (unserialize text)))
            (for-each (match-lambda
                        ((key . value)
                         (hash-set! (.table o) key value)))
                      alist))))))
