;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Initially imported from sneek; <https://gitlab.com/dalepsmith/sneek>.
;;;
;;; Code:

(define-module (snuik time)
  #:use-module (srfi srfi-11)
  #:use-module (ice-9 format)
  #:export (format-time))

(define (date-components total-seconds)
  (let* ((s1 (inexact->exact (round total-seconds)))
         (years (quotient s1 31104000))
         (s2 (- s1 (* years 31104000)))
         (months (quotient s2 2592000))
         (s3 (- s2 (* months 2592000)))
         (days (quotient s3 86400))
         (s4 (- s3 (* days 86400)))
         (hours (quotient s4 3600))
         (s5 (- s4 (* hours 3600)))
         (minutes (quotient s5 60))
         (seconds (- s5 (* minutes 60))))
    (values years months days hours minutes seconds)))

(define (format-relative total-seconds)
  (let-values (((years months days hours minutes seconds)
                (date-components total-seconds)))
    (cond
     ;; years
     ((> years 1)
      (format #f "~d years" years))
     ((= years 1)
      (if (= months 0)
          "one year"
          (format #f "one year and ~d month~:p" months)))

     ;; months
     ((> months 1)
      (format #f "~d months" months))
     ((= months 1)
      (if (= days 0)
          "one month"
          (format #f "one month and ~d day~:p" days)))

     ;; days
     ((> days 1)
      (format #f "~d days" days))
     ((= days 1)
      (if (= hours 0)
          "one day"
          (format #f "one day and ~d hour~:p" hours)))

     ;; hours
     ((>= hours 6)
      (format #f "~d hours" hours))
     ((>= hours 1)
      (if (= minutes 0)
	  (format #f "~d hour~:p" hours)
          (format #f "~d hour~:p and ~d minute~:p" hours minutes)))

     ;; minutes and seconds
     ((= minutes 1)
      (if (> seconds 0)
          (format #f "one minute and ~d second~:p" seconds)
          "one minute"))

     ((= minutes 0)
      (format #f "~d second~:p" seconds))

     ((= seconds 0)
      (format #f "~d minute~:p" minutes))

     (else
      (format #f "~d minute~:p and ~d second~:p" minutes seconds)))))

(define (format-time seen-at)
  (format-relative (- (current-time) seen-at)))
