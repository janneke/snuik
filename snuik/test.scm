;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik test)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 string-fun)
  #:use-module ((oop goops) #:select (class-slots slot-bound?
                                                  slot-definition-name
                                                  slot-set!))
  #:use-module (snuik goops goops)
  #:use-module (snuik handler)
  #:use-module (snuik snuik)
  #:use-module (snuik util)
  #:use-module (snuik irc)
  #:use-module (snuik handler test)
  #:export (<test:snuik>
            %<handler:test>-tests
            %test-port
            %test-channel
            %test-nick
            bind-slot*!
            misc
            notice
            privmsg)
  #:re-export (clone
               send-message
               send-one
               send-result))

(define %test-port 16667)
(define %test-channel "#test")
(define %test-nick "test")

(define-class-public <test:snuik> (<snuik>))

(define (bind-slot*! o)
  (let* ((class (class-of o))
         (slots (class-slots class))
         (names (map slot-definition-name slots))
         (unbound (filter (negate (cute slot-bound? o <>)) names)))
    (for-each (cute slot-set! o <> #f) unbound)
    o))

(define-method (clone (o <test:snuik>) . rest)
  (next-method)
  (slot-set! o 'socket #f)
  o)

(define-method (send-message (o <test:snuik>) channel message)
  (format #f "~a:~a\n" channel message))

(define-method (send-one (o <test:snuik>) channel lines)
  (send-message channel (car lines)))

(define-method (send-result result)
  (match result
    ((and line (? string?))
     (string-trim-right line))
    (((and choices (? string?)) ...)
     (map send-result choices))
    ((and multiple-choices (((? string?) ...) ...))
     (map send-result multiple-choices))
    (_ result)))

(define (misc handler snuik line)
  (send-result (notice handler snuik line)))

(define (notice handler snuik line)
  (let ((message (irc:line->message line)))
    (send-result (handle-message- handler snuik message))))

(define* (privmsg handler snuik speaker channel line #:key emote?)
  (send-result
   (message handler snuik 'PRIVMSG line channel speaker emote?)))

(define* (message handler snuik command message channel speaker emote?)
  (let* ((prefix (format #f "~a!~a@localhost" speaker speaker))
         (line (format #f "~a :~a" prefix message))
         (params (list message))
         (private? (not (string-prefix? "#" channel)))
         (irc-message (make-irc:message
                       line command prefix params
                       speaker channel message emote? private?)))
    (handle-message- handler snuik irc-message)))

(define* (handle-message- handler snuik message)
  (let ((message-for-me (message-for-me? snuik message)))
    (or (and message-for-me
             (handle-message-for-me handler snuik message-for-me))
        (handle-message handler snuik message))))


;;;
;;; <handler:test>
;;;
(define compare-reply (@@ (snuik handler test) compare-reply))

(define (try-format template . args)
  (let ((template (or (false-if-exception
                       (apply format #f template args))
                      template)))
    (string-replace-substring template "<snuik>" "snuik")))

(define %help-alist (@@ (snuik handler help) %help-alist))
(define (help-instruction index)
  (let ((entry (list-ref %help-alist index)))
    (apply string-join entry '(" - "))))

(define %<handler:test>-tests
  (let* ((help-responses (@@ (snuik handler help) %help-responses))
         (help-responses (map (cute try-format <> %test-nick)
                              help-responses))
         (help-introductions (@@ (snuik handler help) %help-introductions))
         (seen-preludes (@@ (snuik handler seen) %seen-preludes))
         (seen-preludes (map (cute format #f <> %test-nick) seen-preludes))
         (hello-message (format #f "hello ~a!" %test-channel))
         (seen-message hello-message)
         (seen-message "snuik: botsnack!")
         (seen-responses (map (cute format #f
                                    "~a in ~a 0 seconds ago, saying: ~a"
                                    <> %test-channel seen-message)
                              seen-preludes))
         (tell-message hello-message)
         (deliver-preludes (@@ (snuik handler tell) %deliver-preludes))
         (deliver-preludes (map (cute format #f <> %test-nick)
                                deliver-preludes))
         (deliver-responses (map (cute format #f "~a you have 1 message" <>)
                                 deliver-preludes))
         (uptime-preludes (@@ (snuik handler uptime) %uptime-preludes))
         (uptime-responses (map (cute format #f "~a 0 seconds" <>)
                                uptime-preludes))
         (what-preludes (@@ (snuik handler info) %what-preludes))
         (what-responses (lambda (message)
                           (map (cute string-append <> message)
                                what-preludes)))
         (whut-responses (@@ (snuik handler whut) %whut-responses)))
    `(,(make <test> #:name "greet"
             #:message "hello snuik!"
             #:reply (format #f "Oh hi ~a!" %test-nick))
      ,(make <test> #:name "whut/greet again"
             #:message "hello again, snuik!"
             #:reply whut-responses)
      ,(make <test> #:name "feed"
             #:message "snuik: botsnack!"
             #:reply (@@ (snuik handler feed) %feed-responses))
      ,(make <test> #:name "sneeky snuik feed"
             #:channel "snuik"
             #:message "botsnack"
             #:reply (@@ (snuik handler feed) %feed-responses))
      ,(make <test> #:name (format #f "snuik: seen ~a?" %test-nick)
             #:message (format #f "snuik: seen ~a?" %test-nick)
             #:reply seen-responses)
      ,(make <test> #:name "not seen"
             #:message "snuik: seen foo?"
             #:reply (@@ (snuik handler seen) %not-seen-responses))
      ,(make <test> #:name "help"
             #:message "snuik: help!"
             #:reply help-responses)
      ,(make <test> #:name "help"
             #:channel "snuik"
             #:message "help!"
             #:reply "Hello, I'm snuik, a Snuiky IRC Channel Bot.")
      ,(make <test> #:name "help"
             #:message "... intro 2"
             #:reply (second help-introductions))
      ,@(map (lambda (i)
               (make <test> #:name "help"
                     #:message (format #f "...help ~a" i)
                     #:reply (help-instruction i)))
             (iota (length %help-alist)))
      ,(make <test> #:name "tell"
             #:message (format #f "snuik: later tell ~a ~a"
                               %test-nick tell-message)
             #:reply (@@ (snuik handler tell) %tell-responses))
      ,(make <test> #:name "tell welcome"
             #:message tell-message
             #:reply deliver-responses)
      ,(make <test> #:name "tell deliver"
             #:message tell-message
             #:reply (format #f "~a, ~a says: ~a"
                             %test-nick %test-nick tell-message))
      ,(make <test> #:name "info/whut foo?"
             #:message "snuik: foo?"
             #:reply "foo?")
      ,(make <test> #:name "info/uptime/whut uptime?"
             #:message "snuik: uptime?"
             #:reply "uptime?")
      ,(make <test> #:name "uptime"
             #:message "snuik: uptime"
             #:reply uptime-responses
             #:compare
             (lambda (reply expected . rest)

               (let ((reply (or (false-if-exception
                                 (regexp-substitute
                                  #f
                                  (string-match "[0-9]+" reply)
                                  'pre "0" 'post))
                                reply)))
                 (apply compare-reply reply expected rest))))
      ,(make <test> #:name "info remember"
             #:message "snuik: foo is foo bar baz"
             #:reply (@@ (snuik handler info) %remember-responses))
      ,(make <test> #:name "info what"
             #:message "snuik: foo?"
      #:reply (what-responses "foo bar baz")))))
