;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik handler)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module ((oop goops) #:select (initialize slot-set!))
  #:use-module (snuik goops goops)
  #:use-module (snuik goops util)
  #:use-module (8sync)
  #:use-module (8sync systems irc)
  #:use-module (snuik irc)
  #:use-module (snuik store)
  #:use-module (snuik util)
  #:export (cleanup
            echo-line
            init
            handle-message-for-me)
  #:re-export (<irc-bot>
               dump
               handle-message))

(define-class*-public <handler> ())

(define-method (init (o <handler>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: init...\n" type)))

(define-method (cleanup (o <handler>) (bot <irc-bot>))
  (let ((type (class-name (class-of o))))
    (format (current-error-port) "[~a]: cleaning up...\n" type)))

(define-method (echo-line (o <handler>) message)
  (let ((class (class-of o)))
    (when (eq? class <handler>)
      (let ((type (class-name class)))
        (match message
          ((and ($ <irc:message>) (= irc:message-line line))
           (format (current-error-port) "[~a]: ~a\n" type line)))))))

(define-method (handle-message (o <handler>) (bot <irc-bot>) message)
  "Fallback handler, print line."
  (echo-line o message)
  ;; line not (really) handled
  #f)

(define-method (handle-message-for-me (o <handler>) (bot <irc-bot>) message)
  "Fallback handler, print line."
  (echo-line o message)
  ;; line not (really) handled
  #f)

(define-class*-public <handler:with-store> (<handler>)
  (store #:init-form (make <store>)))

(define-method (initialize (o <handler:with-store>) initargs)
  (next-method)
  (let-keywords initargs #t
                ((store #f))
    (if store (slot-set! o 'store store)
        (initialize (.store o) initargs)))
  o)

(define-method (dump (o <handler:with-store>))
  (dump (.store o)))

(define-method (retrieve (o <handler:with-store>))
  (retrieve (.store o)))
