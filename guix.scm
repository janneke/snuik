;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2025 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;
;; GNU Guix development package.  To build and install, run:
;;
;;   guix package -f guix.scm
;;
;; To build it, but not install it, run:
;;
;;   guix build -f guix.scm
;;
;; To use as the basis for a development environment, run:
;;
;;   guix shell -D -f guix.scm
;;
;; or simply
;;
;;   guix shell
;;
;;; Code:

(use-modules (srfi srfi-1)
             (guix gexp)
             (guix git-download)
             (guix packages)
             (gnu packages)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages messaging)
             (gnu packages texinfo))

(define %source-dir (dirname (current-filename)))
(add-to-load-path (string-append %source-dir "/guix"))
(%patch-path (cons (string-append %source-dir "/guix") (%patch-path)))
(use-modules (pack snuik))

(define-public snuik.git
  (package
    (inherit snuik)
    (version "git")
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (native-inputs `(("make" ,gnu-make) ;for development
                     ("ngircd" ,ngircd) ;for live test/snuik.scm test
                     ("texinfo" ,texinfo)
                     ,@(package-native-inputs snuik)))))

snuik.git
