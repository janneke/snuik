;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2020, 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (snuik)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-37)

  #:use-module (8sync)
  #:use-module (8sync repl)

  #:use-module (snuik snuik)
  #:use-module (snuik handler)
  #:use-module (snuik handler alive)
  #:use-module (snuik handler feed)
  #:use-module (snuik handler greet)
  #:use-module (snuik handler help)
  #:use-module (snuik handler identify)
  #:use-module (snuik handler info)
  #:use-module (snuik handler seen)
  #:use-module (snuik handler tell)
  #:use-module (snuik handler uptime)
  #:use-module (snuik handler whut)

  #:use-module (snuik goops goops)
  #:use-module (snuik util)

  #:export (main))

(define %default-server "irc.libera.chat")
(define %default-channels '("##botchat"))
(define %default-nick "snuik")

(define-actor <sleeper> (<actor>)
  ((*init* sleeper-loop))
  (ping #:init-form (list) #:getter .ping #:init-keyword #:ping)
  (seconds #:init-value 4 #:getter .seconds #:init-keyword #:seconds)
  (verbose? #:init-value 4 #:getter .verbose? #:init-keyword #:verbose?))

(define (sleeper-loop actor message)
  (while (actor-alive? actor)
    (8sleep (.seconds actor))
    (for-each (cute <-wait <> 'ping (.seconds actor)) (.ping actor))
    (when (.verbose? actor)
      (format (current-error-port) "-- MARK --\n")
      (flush-all-ports))))

(define* (display-help script-name #:key (port (current-output-port)))
  (format port "Usage: ~a [OPTION]" script-name)
  (format port "
  -h, --help                  display this text
      --server=SERVER         connect to SERVER [~a]
      --port=PORT             connect to port [~a]
      --nick=NICK             use nick NICK [~a]
      --password=PASSWORD     identify using PASSWORD [read: .password.<nick>]
      --channels=CHANNEL1[,CHANNEL2...]
                              join comma-separated list of channels on connect
                              [~a]
      --store-directory=DIR   use directory DIR for the store [~a]
      --without-store"
          %default-server
          %irc:default-port
          %default-nick
          (string-join %default-channels ",")
          "store")
  (newline port))

(define (parse-args script-name args)
  (args-fold (cdr args)
             (list (option '(#\h "help") #f #f
                           (lambda _
                             (display-help script-name)
                             (exit 0)))
                   (option '("server") #t #f
                           (lambda (opt name arg result)
                             `(#:server ,arg ,@result)))
                   (option '("port") #t #f
                           (lambda (opt name arg result)
                             (let ((port (string->number arg)))
                               `(#:port ,port ,@result))))
                   (option '("password") #t #f
                           (lambda (opt name arg result)
                             `(#:password ,arg ,@result)))
                   (option '("store-directory") #t #f
                           (lambda (opt name arg result)
                             `(#:store-directory ,arg ,@result)))
                   (option '("without-store") #f #f
                           (lambda (opt name arg result)
                             `(#:store-directory none ,@result)))
                   (option '("channels") #t #f
                           (lambda (opt name arg result)
                             `(#:channels ,(string-split arg #\,)
                               ,@result)))
                   (option '("nick") #t #f
                           (lambda (opt name arg result)
                             `(#:nick ,arg ,@result)))
                   (option '("repl") #f #t
                           (lambda (opt name arg result)
                             `(#:repl ,(or arg #t) ,@result))))
             (lambda (opt name arg result)
               (format (current-error-port) "Unrecognized option `~a'\n" name)
               (display-help script-name #:port (current-error-port))
               (exit 2))
             (lambda (file result)
               (format (current-error-port) "Extra argument: `~a'\n" file)
               (display-help script-name #:port (current-error-port))
               (exit 2))
             '()))

(define* (run-bot #:key
                  (channels (list %default-channels))
                  (nick %default-nick)
                  (password (password nick))
                  (port %irc:default-port)
                  (repl "/tmp/snuik-repl")
                  (server %default-server)
                  (store-directory "store"))
  (let* ((hive (make-hive))
         (handler (make <handler>))
         (identify-handler (make <handler:identify>))
         (alive-handler (make <handler:alive>))
         (tell-handler (make <handler:tell> #:store-directory store-directory))
         (seen-handler (make <handler:seen> #:store-directory store-directory))
         (feed-handler (make <handler:feed>))
         (greet-handler (make <handler:greet>))
         (info-handler (make <handler:info> #:store-directory store-directory))
         (uptime-handler (make <handler:uptime>))
         (help-handler (make <handler:help>))
         (whut-handler (make <handler:whut>))
         (handlers (list handler
                         identify-handler
                         alive-handler
                         tell-handler
                         seen-handler
                         feed-handler
                         greet-handler
                         info-handler
                         uptime-handler
                         help-handler
                         whut-handler))
         (snuik (bootstrap-actor* hive <snuik> "snuik"
                                  #:username nick
                                  #:realname "Snuiky IRC Channel Bot"
                                  #:server server
                                  #:port port
                                  #:channels channels
                                  #:handlers handlers
                                  #:password password
                                  #:store-directory store-directory))
         ;; M-x geiser-connect-local <RET> guile <RET> /tmp/snuik-repl <RET>, or
         ;; M-x geiser-connect-local <RET> /tmp/snuik-repl <RET>
         (repl-manager (and repl
                            (bootstrap-actor* hive <repl-manager> "repl" #:path repl)))
         (sleeper (bootstrap-actor hive <sleeper> #:seconds 60 #:verbose? #t
                                   #:ping (list snuik))))
    (run-hive hive '())))

(define (main args)
  (let ((parsed-args (parse-args "snuik" args)))
    (apply run-bot parsed-args)))
