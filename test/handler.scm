;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test handler)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (build-aux test automake)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler feed)
  #:use-module (snuik handler greet)
  #:use-module (snuik handler uptime)
  #:use-module (snuik handler whut)
  #:use-module (snuik handler help)
  #:use-module (snuik util)

  #:use-module (snuik test))

(test-begin "test/handler")

(test-assert "dummy"
  #t)

(let* ((nick "snuik")
       (pass "pass")
       (speaker "speaker")
       (channel "#channel")
       (snuik (make <test:snuik> #:username nick #:password pass
                    #:realname "test bot"))
       (snuik (bind-slot*! snuik)))

  (let* ((uptime (make <handler:uptime> #:uptime-preludes '("uptime")))
         (snuik (clone snuik #:handlers (list uptime))))

    (test-assert "[uptime] Welcome!"
      (not
       (misc uptime snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[uptime] snuik: burp"
      (not (privmsg uptime snuik speaker channel (format #f "~a: burp" nick))))

    (test-equal "[uptime] snuik: uptime?"
      (map (cute string-append <> " 0 seconds") (.uptime-preludes uptime))
      (privmsg uptime snuik speaker channel (format #f "~a: uptime?" nick)))

    (test-equal "[uptime] <private> uptime?"
      (map (cute string-append <> " 0 seconds") (.uptime-preludes uptime))
      (privmsg uptime snuik speaker nick "uptime?")))

  (let* ((feed (make <handler:feed> #:feed-responses '(":-)")))
         (snuik (clone snuik #:handlers (list feed))))

    (test-assert "[feed] Welcome!"
      (not
       (misc feed snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[feed] snuik: burp"
      (not (privmsg feed snuik speaker channel (format #f "~a: burp" nick))))

    (test-equal "[feed] snuik: botsnack"
      (.feed-responses feed)
      (privmsg feed snuik speaker channel (format #f "~a: botsnack" nick)))

    (test-equal "[feed] snuik: botsnack"
      (.feed-responses feed)
      (privmsg feed snuik speaker channel (format #f "~a: botsnack!" nick))))

  (let* ((greet (make <handler:greet>
                  #:greet-responses '("greet ~a")
                  #:morning-responses '("morning ~a")
                  #:night-responses '("night ~a")))
         (snuik (clone snuik #:handlers (list greet))))

    (test-assert "[greet] Welcome!"
      (not
       (misc greet snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[greet] snuik: burp"
      (not (privmsg greet snuik speaker channel (format #f "~a: burp" nick))))

    (test-equal "[greet] snuik: hi!"
      (map (cute format #f <> speaker) (.greet-responses greet))
      (privmsg greet snuik speaker channel (format #f "~a: hi!" nick)))

    (test-equal "[greet] morning, snuik!"
      (map (cute format #f <> speaker) (.morning-responses greet))
      (privmsg greet snuik speaker channel (format #f "morning, ~a!" nick)))

    (test-equal "[greet] good night snuik"
      (map (cute format #f <> speaker) (.night-responses greet))
      (privmsg greet snuik speaker channel (format #f "good night ~a" nick))))

  (let* ((whut (make <handler:whut> #:whut-responses '("whut")))
         (snuik (clone snuik #:handlers (list whut))))

    (test-assert "[whut] Welcome!"
      (not
       (misc whut snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-equal "[whut] snuik: burp"
      (.whut-responses whut)
      (privmsg whut snuik speaker channel (format #f "~a: burp" nick)))

    (test-equal "[whut] snuik: whut?"
      (.whut-responses whut)
      (privmsg whut snuik speaker channel (format #f "~a: whut?" nick))))

  (let* ((help (make <handler:help> #:help-introductions '("hi")
                     #:help-alist '(("help" "commands"))
                     #:help-responses '("try /msg")))
         (snuik (clone snuik #:handlers (list help))))

    (test-assert "[help] Welcome!"
      (not
       (misc help snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[help] snuik: burp"
      (not (privmsg help snuik speaker channel (format #f "~a: burp" nick))))

    (test-equal "[help] snuik: help?"
      (.help-responses help)
      (privmsg help snuik speaker channel (format #f "~a: help?" nick)))

    (test-equal "[help] <private> help?"
      (string-append (string-join (.help-introductions help) "\n")
                     "\n"
                     (string-join (car (.help-alist help)) " - "))
      (privmsg help snuik speaker nick "help?"))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
