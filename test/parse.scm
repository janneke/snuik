;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test snuik)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 match)

  #:use-module (snuik irc)
  #:use-module (snuik util)
  #:use-module (snuik test))

(define (line->message line)
  (let ((message (irc:line->message line)))
    (match message
      ((and ($ <irc:message>)
            (= irc:message-line line)
            (= irc:message-command command)
            (= irc:message-prefix prefix)
            (= irc:message-params params)
            (= irc:message-speaker speaker)
            (= irc:message-channel channel)
            (= irc:message-message message)
            (= irc:message-emote? emote?)
            (= irc:message-private? private?))
       (when line
         (format #t  "     line: ~a\n" line))
       (when command
         (format #t  "  command: ~s\n" command))
       (unless (or (not speaker)
                   (equal? speaker prefix))
         (format #t  "  speaker: ~s\n" speaker))
       (when channel
         (format #t  "  channel: ~s\n" channel))
       (when message
         (format #t  "  message: ~s~a~a\n" message
                 (if emote? " *emotes*" "")
                 (if private? " *private*" ""))))
      (_ (format #t "parse error: ~a\n" line)))
    message))

(test-begin "test/parse")

(let* ((text (with-input-from-file "etc/messages.log" read-string))
       (text (string-trim-both text))
       (lines (string-split text #\newline)))
  (for-each (lambda (line)
              (format #t "\n~a\n" line)
              (test-assert
                (irc:message? (line->message line))))
            lines))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
