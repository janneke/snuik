;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test snuik)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)

  #:use-module (8sync)

  #:use-module (snuik handler)
  #:use-module (snuik snuik)
  #:use-module (snuik goops goops)
  #:use-module (snuik util)
  #:use-module (snuik handler test)
  #:use-module (snuik test))

(define run-bot (@@ (snuik) run-bot))

(define %next
  (let ((count -1))
    (lambda _
      (set! count (1+ count))
      (format #f "~a" 0))))

(define (listening? port)
  "Check if something is listening on localhost:PORT."
  (let* ((sock (socket PF_INET SOCK_STREAM 0))
         (address (make-socket-address AF_INET INADDR_LOOPBACK port)))
    (and sock
         (catch 'system-error
           (lambda _ (connect sock address))
           (const #f)))))

(define* (wait-for-port port #:key (seconds 10))
  "Wait SECONDS for localhost:PORT to accept connections."
  (let loop ((i seconds))
    (cond ((listening? port)
           #t)
          ((> i 0)
           (sleep 1)
           (loop (- i 1)))
          (else
           (error "Socket didn't show up: " port)))))

(define* (run-test-bot #:key
                       (channels (list %test-channel))
                       (nick %test-nick)
                       password
                       (port %test-port)
                       repl
                       (server "localhost")
                       (driver (const (display "running...")))
                       (handlers (list
                                  (make <handler>)
                                  (make <handler:test> #:driver driver))))
  (let* ((hive (make-hive))
         (snuik (bootstrap-actor* hive <snuik> "*test*"
                                  #:username nick
                                  #:realname "test bot"
                                  #:server server
                                  #:port port
                                  #:channels channels
                                  #:handlers handlers
                                  #:password password)))
    (run-hive hive '())))

(eval-when (eval)
  (unless (search-path (string-split (getenv "PATH") #\:) "ngircd")
    (let ((location (current-source-location)))
      (apply format (current-error-port)
             "~a:~a:~a:warning: ngircd not found, skipping live test.\n"
             (map cdr location)))
    (exit 0)))

(test-begin "test/snuik")

(match (primitive-fork)
  (0
   (execlp "ngircd" "ngircd" "-n" "-f" "etc/ngircd.conf"))
  (ircd-pid
   (format (current-error-port) "ircd-pid: ~a\n" ircd-pid)

   (match (primitive-fork)
     (0
      (let ()
        (define (cleanup)
          (sigaction SIGCHLD SIG_DFL)

          (format (current-error-port) "killing ircd [~a]...\n" ircd-pid)
          (kill ircd-pid SIGINT))

        (define (signal-handler signal)
          (format (current-error-port) "caught signal ~a\n" signal)
          (cleanup)
          (exit 1))

        ;; Try to not leave a mess.
        (sigaction SIGCHLD signal-handler)

        (wait-for-port %test-port)
        (run-bot #:channels (list %test-channel)
                 #:password "pass"
                 #:port %test-port
                 #:repl #f
                 #:server "localhost"
                 #:store-directory 'none)

        (format (current-error-port) "test bot done...\n")
        (exit 0)))
     (snuik-pid

      (test-assert "dummy"
        #t)

      (let ((test-handler (make <handler:test>
                            #:channel %test-channel
                            #:tests %<handler:test>-tests)))
        (define (cleanup)
          (sigaction SIGCHLD SIG_DFL)
          (sigaction SIGHUP SIG_DFL)
          (sigaction SIGINT SIG_DFL)
          (sigaction SIGTERM SIG_DFL)

          (format (current-error-port) "killing ircd [~a]...\n" ircd-pid)
          (kill ircd-pid SIGINT)
          (format (current-error-port) "killing snuik [~a]...\n" snuik-pid)
          (kill snuik-pid SIGINT))

        (define (signal-handler signal)
          (format (current-error-port) "caught signal ~a\n" signal)
          (cleanup))

        ;; Try to not leave a mess.
        (sigaction SIGCHLD signal-handler)
        (sigaction SIGHUP signal-handler)
        (sigaction SIGINT signal-handler)
        (sigaction SIGTERM signal-handler)

        (format (current-error-port) "snuik-pid: ~a\n" snuik-pid)
        (format (current-error-port) "test-pid: ~a\n" (getpid))

        (wait-for-port %test-port)
        (format (current-error-port) "running test bot\n")
        (test-eq "live: snuik"
          0
          (run-test-bot #:handlers (list (make <handler>)
                                         test-handler)))

        (format (current-error-port) "test bot exited\n")

        (test-eq "live: handlers"
          0
          (.fail test-handler))

        (sleep 1)
        (let* ((pass (.pass test-handler))
               (fail (.fail test-handler))
               (total (+ pass fail)))
          (format (current-error-port)
                  "LOG>>>>>>>>>>>>>>>>>>>>\n~a<<<<<<<<<<<<<<<<<<<<\n"
                  (apply string-append (.log test-handler)))
          (format (current-error-port) "TOTAL: ~a\n" total)
          (format (current-error-port) "PASS:  ~a\n" pass)
          (format (current-error-port) "FAIL:  ~a\n" fail)

          (format (current-error-port) "test bot exited\n")

          (cleanup)

          (format (current-error-port) "waiting for childs to exit...\n")
          (waitpid -1)
          (format (current-error-port) "exiting...\n")
          (exit (zero? (.fail test-handler)))))))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
