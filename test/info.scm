;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test info)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (build-aux test automake)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler info)
  #:use-module (snuik store)
  #:use-module (snuik util)

  #:use-module (snuik test))

(test-begin "test/info")

(test-assert "dummy"
  #t)

(let* ((nick "snuik")
       (pass "pass")
       (speaker "speaker")
       (channel "#channel"))

  (let* ((info-alist `(("bar" . "baz")))
         (info-table (alist->hash-table info-alist))
         (info-store (make <store> #:table info-table))
         (info (make <handler:info> #:store info-store
                     #:what-preludes '("what:")
                     #:remember-responses '("remember")
                     #:know-responses '("know")
                     #:change-responses '("change:")
                     #:forget-responses '("forget:")
                     #:amnesia-responses '("amnesia:")))
         (snuik (make <test:snuik> #:username nick #:password pass))
         (snuik (bind-slot*! snuik))
         (snuik (clone snuik #:handlers (list info))))

    (define (what-response message)
      (map (cute string-append <> message) (.what-preludes info)))

    (test-assert "[info] Welcome!"
      (not
       (misc info snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[info] snuik: burp"
      (not (privmsg info snuik speaker channel (format #f "~a: burp" nick))))

    (begin ;;test-equal "[info] snuik: foo?"
      "foo?"
      (privmsg info snuik speaker channel (format #f "~a: foo?" nick)))

    (for-each
     (lambda (query )
       (begin ;;test-equal (string-append "[info] " nick query)
         (what-response (assoc-ref info-alist "bar"))
         (privmsg info snuik speaker channel (string-append nick query))))
     '(" bar?"
       " bar ?"
       " what bar"
       " what bar?"
       " what bar ?"
       " what is bar"
       " what is bar?"
       " what is bar ?"

       ": bar?"
       ": bar ?"
       ": what bar"
       ": what bar?"
       ": what bar ?"
       ": what is bar"
       ": what is bar?"
       ": what is bar ?"))

    (test-assert "[info] snuik: bar"
      (not
       (privmsg info snuik speaker channel (string-append nick ": bar"))))

    (test-assert "[info] bar?"
      (not
       (privmsg info snuik speaker channel "bar?")))

    (test-equal "[info] <private> bar?"
      (what-response (assoc-ref info-alist "bar"))
      (privmsg info snuik speaker nick "bar?"))

    (test-equal "[info] snuik: remember bla bla bla blah"
      (.remember-responses info)
      (privmsg info snuik speaker channel
               "snuik: remember bla bla bla blah"))

    (test-equal "[info] snuik: bla is bla bla blah"
      (.know-responses info)
      (privmsg info snuik speaker channel
               "snuik: bla is bla bla blah"))

    (let ((thing "bla")
          (message "bla bla blah"))
      (test-equal (string-append "[info] snuik: remember " thing " " message)
        (.know-responses info)
        (privmsg info snuik speaker channel
                 (string-append "snuik: remember " thing " " message)))

      (test-equal "[info] snuik: bla?"
        (map (cute string-append <> message) (.what-preludes info))
        (privmsg info snuik speaker channel
                 (format #f "~a: ~a?" nick thing)))

      (test-equal (string-append "[info] snuik: forget " thing)
        (.forget-responses info)
        (privmsg info snuik speaker channel
                 (format #f "~a: forget ~a" nick thing)))

      (test-equal "[info] snuik: bla? (forgotten)"
        "bla?"
        (privmsg info snuik speaker channel
                 (format #f "~a: ~a?" nick thing))))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
