;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test test)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (build-aux test automake)
  #:use-module (ice-9 match)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler test)
  #:use-module (snuik store)
  #:use-module (snuik util)

  #:use-module (snuik test))

(define %test-nick "test")
(define %test-channel "#test")

(test-begin "test/test")

(test-assert "dummy"
  #t)

(let* ((nick "snuik")
       (pass "pass")
       (speaker "speaker")
       (channel "#channel"))

  (let* ((test (make <handler:test> #:kill? #f))
         (snuik (make <test:snuik> #:username nick #:password pass))
         (snuik (bind-slot*! snuik))
         (snuik (clone snuik #:handlers (list test))))

    (test-equal "[test] Welcome!"
      (.lurk-responses test)
      (misc test snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost"))

    (test-equal "[test] snuik: burp"
      (.talk-responses test)
      (privmsg test snuik speaker channel (format #f "~a: burp" nick))))

  (let* ((tests %<handler:test>-tests)
         (test (make <handler:test> #:kill? #f #:tests tests))
         (snuik (make <test:snuik> #:username nick #:password pass))
         (snuik (bind-slot*! snuik))
         (snuik (clone snuik #:handlers (list test))))

    (test-assert "[test] 353, aka NAMES, aka tests #-1"
      (not (misc test snuik
                 (format #f ":foo.bar 353 ~a @ ~a :~a snuik"
                         %test-nick %test-channel %test-nick))))

    (let loop ((tests tests) (count 0))
      (match tests
        ((current next rest ...)
         (test-equal (format #f "[test] tests #~a" count)
           (string-append (.channel next) ":" (.message next))
           (privmsg test snuik speaker (.channel current) (.message current)))
         (loop (cons next rest) (1+ count)))
        ((current)
         current
         (test-equal "[test] test1"
           (.talk-responses test)
           (privmsg test snuik speaker
                    (.channel current) (.message current))))))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
