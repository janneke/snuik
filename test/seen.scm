;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test handler seen)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (build-aux test automake)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler seen)
  #:use-module (snuik store)
  #:use-module (snuik util)

  #:use-module (snuik test))

(test-begin "test/seen")

(test-assert "dummy"
  #t)

(let* ((nick "snuik")
       (pass "pass")
       (speaker "speaker")
       (channel "#channel")
       (snuik (make <test:snuik> #:username nick #:password pass))
       (snuik (bind-slot*! snuik)))

  (let* ((seen-alist '(("sue" . "0")))
         (seen-table (alist->hash-table seen-alist))
         (seen-store (make <store> #:table seen-table))
         (seen (make <handler:seen> #:seen-store seen-store
                     #:seen-preludes '("[seen]:")
                     #:not-seen-responses '("not-seen:")))
         (snuik (clone snuik #:handlers (list seen))))

    (define (set-seen! nick channel message emote?)
      ((@@ (snuik handler seen) set-seen!) seen nick channel message emote?))

    (define (get-seen nick)
      ((@@ (snuik handler seen) get-seen) seen nick))

    (define (seen? line)
      ((@@ (snuik handler seen) seen?) (string-trim-both line)))

    (set-seen! "sue" channel "i'm a boy" #t)

    (define (seen-response message)
      (map (cute string-append <> " in " channel " 0 seconds ago, " message)
           (.seen-preludes seen)))

    ;; (test-assert "[seen] Welcome!"
    ;;   (not
    ;;    (misc seen snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    ;; (test-assert "[seen] snuik: burp"
    ;;   (not (privmsg seen snuik speaker channel (format #f "~a: burp" nick))))

    ;; (test-equal "[seen] snuik: seen foo?"
    ;;   (.not-seen-responses seen)
    ;;   (privmsg seen snuik speaker channel (format #f "~a: seen foo?" nick)))

    ;; (for-each
    ;;  (lambda (query)
    ;;    (test-equal (string-append "[seen] " nick query)
    ;;      (seen-response "emoting: i'm a boy")
    ;;      (privmsg seen snuik speaker channel (string-append nick query))))
    ;;  '(" seen sue"
    ;;    " seen sue?"
    ;;    " seen sue ?"
    ;;    " sue seen"
    ;;    " sue seen?"
    ;;    " sue seen ?"
    ;;    " seen sue, lately"
    ;;    " seen sue, lately?"
    ;;    " seen sue, lately ?"

    ;;    ": seen sue"
    ;;    ": seen sue?"
    ;;    ": seen sue ?"
    ;;    ": sue seen"
    ;;    ": sue seen?"
    ;;    ": sue seen ?"
    ;;    ": seen sue, lately"
    ;;    ": seen sue, lately?"
    ;;    ": seen sue, lately ?"))

    ;; (test-assert "[seen] bar?"
    ;;   (not
    ;;    (privmsg seen snuik speaker channel (string-append nick ": bar?"))))

    ;; (test-assert "[seen] <private> bar?"
    ;;   (not
    ;;    (privmsg seen snuik speaker nick "bar?")))

    (test-equal "[seen] seen bar? (unseen)"
      (.not-seen-responses seen)
      (privmsg seen snuik speaker channel (string-append nick ": seen bar")))

    (test-assert "[seen] bar emotes hi"
      (not
       (privmsg seen snuik "bar" channel "back" #:emote? #t)))

    (test-equal "[seen] seen bar? (seen)"
      (seen-response "emoting: back")
      (privmsg seen snuik speaker channel (string-append nick ": seen bar?")))

    (test-assert "[seen] baz <private> hi"
      (not
       (privmsg seen snuik "baz" "snuik" "back" #:emote? #t)))

    (test-equal "[seen] seen baz? (seen)"
      (.not-seen-responses seen)
      (privmsg seen snuik speaker channel (string-append nick ": seen baz?")))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
