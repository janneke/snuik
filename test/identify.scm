;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test identify)
  #:use-module (srfi srfi-64)
  #:use-module (build-aux test automake)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler identify)
  #:use-module (snuik util)

  #:use-module (snuik test))

(test-begin "test/identify")

(test-assert "dummy"
  #t)

(let* ((nick "snuik")
       (pass "pass")
       (speaker "speaker")
       (channel "#channel")
       (snuik (make <test:snuik> #:username nick #:password pass))
       (snuik (bind-slot*! snuik)))

  (let* ((identify (make <handler:identify>))
         (snuik (clone snuik #:handlers (list identify))))

    (test-assert "[identify] Welcome!"
      (not
       (misc identify snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[identify] snuik: burp"
      (not (privmsg identify snuik speaker channel (format #f "~a: burp" nick))))

    (test-equal "identify"
      "NickServ:IDENTIFY snuik pass"
      (notice identify snuik
              ":NickServ!foo@host NOTICE snuik :This nickname is registered."))

    (test-assert "identify fail: some notice"
      (not
       (notice identify snuik
               (format #f ":NickServ!foo@host NOTICE ~a :Some notice" nick))))

    (test-assert "identify fail: spoof"
      (not
       (notice
        identify snuik
        (format #f ":Malory!foo@host NOTICE ~a :This nickname is registered."
                nick))))

    (test-assert "identify fail: public channel"
      (not
       (notice
        identify snuik
        (format #f
                ":NickServ!foo@host NOTICE ~a :This nickname is registered."
                channel))))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
