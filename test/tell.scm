;;; Snuik --- An IRC bot using guile-8sync
;;; Copyright © 2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Snuik.
;;;
;;; Snuik is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Snuik is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Snuik.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Commentary:
;;;
;;; Code:

(define-module (test handler tell)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-64)
  #:use-module (build-aux test automake)
  #:use-module (snuik goops goops)
  #:use-module (snuik handler tell)
  #:use-module (snuik store)
  #:use-module (snuik util)

  #:use-module (snuik test))

(test-begin "test/tell")

(test-assert "dummy"
  #t)

(let* ((nick "snuik")
       (pass "pass")
       (speaker "speaker")
       (channel "#channel")
       (snuik (make <test:snuik> #:username nick #:password pass))
       (snuik (bind-slot*! snuik)))

  (let* ((tell-alist '())
         (tell-table (alist->hash-table tell-alist))
         (tell-store (make <store> #:table tell-table))
         (tell (make <handler:tell> #:tell-store tell-store
                     #:tell-responses '("[tell0]:" "tell1")
                     #:deliver-preludes '("deliver:")))
         (snuik (clone snuik #:handlers (list tell))))

    (define (deliver-response channel recipient messages)
      (let* ((count (length messages))
             (messages (map
                        (cute string-append
                              recipient ", " speaker " says: " <>)
                        messages))
             (prelude (if (= count 1) " you have 1 message"
                          (format #f " you have ~a messages" count))))
        (map (cute string-append <> prelude "\n" (string-join messages "\n"))
             (.deliver-preludes tell))))

    (test-assert "[tell] Welcome!"
      (not
       (misc tell snuik ":irc.snuik.net 001 snuik :Welcome to the Internet Relay Network snuik!~snuik@localhost")))

    (test-assert "[tell] snuik: burp"
      (not (privmsg tell snuik speaker channel (format #f "~a: burp" nick))))

    (test-assert "[tell] snuik: later burp foo foo"
      (not (privmsg tell snuik speaker channel (format #f "~a: later burp foo foo" nick))))

    (test-assert "[tell] snuik: never tell foo foo"
      (not (privmsg tell snuik speaker channel (format #f "~a: never tell foo foo" nick))))

    (test-equal "[tell] snuik: later tell foo foo"
      (list (.tell-responses tell))
      (privmsg tell snuik speaker channel (format #f "~a: later tell foo foo" nick)))

    (test-equal "[tell] snuik: deliver foo"
      (list
       (send-result
        (map (cute string-append <> "\n")
             (deliver-response channel "foo" '("foo")))))
      (privmsg tell snuik "foo" channel "back" #:emote? #t))

    (test-equal "[tell] snuik: later tell foo foo"
      (list (.tell-responses tell))
      (privmsg tell snuik speaker channel (format #f "~a: later tell foo foo" nick)))

    (test-equal "[tell] snuik: later tell foo bar"
      (list (.tell-responses tell))
      (privmsg tell snuik speaker channel (format #f "~a: later tell foo bar" nick)))

    (test-equal "[tell] snuik: deliver foo, bar"
      (list
       (send-result
        (map (cute string-append <> "\n")
             (deliver-response channel "foo" '("foo" "bar")))))
      (privmsg tell snuik "foo" channel "back" #:emote? #t))

    (test-equal "[tell] snuik: later tell speaker foo"
      (list (.tell-responses tell))
      (privmsg tell snuik speaker channel
               (format #f "~a: later tell speaker foo" nick)))

    (test-equal "[tell] snuik: deliver foo, later tell speaker bar"
      (list (deliver-response channel "speaker" '("foo"))
            (.tell-responses tell))
      (privmsg tell snuik speaker channel
               (format #f "~a: later tell speaker bar" nick)))))

(let ((result (test-end)))
  (unless (zero? (test-runner-fail-count result))
    (exit 1))
  result)
